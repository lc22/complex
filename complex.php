<?php
class complex{
    public $re, $im;
    public function __construct($re, $im){
        $this->re = (float)$re;
        $this->im = (float)$im;
    }
    public function out(){
        //вывод в читаемой форме
        echo ($this->im >= 0) ? $this->re.' + '.$this->im.'*i' : $this->re.' '.$this->im.'*i';
    }
}