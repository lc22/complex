<?php
require_once 'ops.php';
$c1 = new complex(1, 0);
$c2 = new complex(0.5, 1);
echo 'add: ';
ops::add($c1, $c2)->out();
echo '<br>';
echo 'sub: ';
ops::subtract($c1, $c2)->out();
echo '<br>';
echo 'multiply: ';
ops::multiply($c1, $c2)->out();
echo '<br>';
echo 'divide: ';
ops::divide($c1, $c2)->out();
echo '<br>';