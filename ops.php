<?php
require_once 'complex.php';
class ops{
    public static function isComplex($c){
        //проверка вводных данных
        if(!is_object($c)){
            return false;
        }
        if(!isset($c->re)||!isset($c->im)){
            return false;
        }
        if(!is_numeric($c->re)||!is_numeric($c->im)){
            return false;
        }
        return true;
    }
    public static function add($c1, $c2){
        if(!self::isComplex($c1)||!self::isComplex($c2)){
            return false;
        }
        $re = $c1->re + $c2->re;
        $im = $c1->im + $c2->im;
        return new complex($re, $im);
    }
    public static function subtract($c1, $c2){
        if(!self::isComplex($c1)||!self::isComplex($c2)){
            return false;
        }
        $re = $c1->re - $c2->re;
        $im = $c1->im - $c2->im;
        return new complex($re, $im);
    }
    public static function multiply($c1, $c2){
        if(!self::isComplex($c1)||!self::isComplex($c2)){
            return false;
        }
        $re = ($c1->re)*($c2->re) - ($c1->im)*($c2->im);
        $im = ($c1->im)*($c2->re) + ($c2->im)*($c1->re);
        return new complex($re, $im);
    }
    public static function divide($c1, $c2){
        if(!self::isComplex($c1)||!self::isComplex($c2)){
            return false;
        }
        if(($c2->re == 0)&&($c2->im == 0)){ //проверяем чтобы не было деления на 0
            return false;
        }
        $r2 = ($c2->re)*($c2->re) + ($c2->im)*($c2->im);
        $re = (($c1->re)*($c2->re) + ($c1->im)*($c2->im))/$r2;
        $im = ($c1->im*$c2->re -$c1->re*$c2->im)/$r2;
        return new complex($re, $im);
    }
}